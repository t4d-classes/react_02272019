# Welcome to React Class

## Instructor

Eric Greene

## Schedule

Class:

- Wednesday - Thursday, 9am to 5pm PST

Breaks:

- Morning: 10:25am to 10:35am
- Lunch: Noon to 1pm
- Afternoon #1: 2:05pm to 2:15pm
- Afternoon #2: 3:15pm to 3:25pm

## Course Outline

- Day 1 - What is React, Functional Components, JSX, Props, Default Props, State Hook, Composition
- Day 2 - Classes, Lifecycle Functions (Effect Hooks), Keys, Refs (Ref Hook), Memo, Unit Testing

### Requirements

- Node.js (version 10 or later)
- Web Browser
- Text Editor

### Instructor's Resources

- [DevelopIntelligence](http://www.developintelligence.com/)

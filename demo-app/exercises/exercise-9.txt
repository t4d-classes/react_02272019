Exercise 9

1. Create a new component named EditCarRow. The Edit Car Row component is similar to Car View Row, it display the car data in a table row, but each of the row is an input field. The last column has two buttons, Save and Cancel. The user should be able to edit the car in the fields, but the changes cannot be saved or canceled. The id should not be editable.

2. Add an Edit button to the same column where Delete button exists in Car View Row. When the Edit button is clicked, the view row for that one car, changes to the edit row. Only one row is editable at a time. The fields should be populated with the current car information.

3. Do not implement the onClick for the Save and Cancel button, but display the buttons as part of Car Edit Row.

4. Implement the code for the Save and Cancel buttons. If the Save is clicked, save the car changes and switch the edit row to view row. If the Cancel is clicked, do not save the car changes and switch from edit row to view.

5. If a row is being edited, and the delete button on another row is clicked, switch the edit row to a view row and perform the delete as usual.

6. If a row is being edited, and 'Add Car' button is clicked, switch the edit row to a view row and add the car as usual.

7. Ensure it works.




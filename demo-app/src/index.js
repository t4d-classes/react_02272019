import React from 'react';
import ReactDOM from 'react-dom';

import { ColorTool } from './components/ColorTool';
import { CarTool } from './components/CarTool';

const colorList = [ 'blue', 'yellow', 'orange', 'hot pink', 'green' ];

const carList = [
  { id: 1, make: 'Ford', model: 'Fusion Hybrid', color: 'blue', year: 2018, price: 30000 },
  { id: 2, make: 'Tesla', model: 'Model S', color: 'red', year: 2017, price: 100000 },
];

ReactDOM.render(
  <>
    {/* <ColorTool colors={colorList} /> */}
    <CarTool cars={carList} />
  </>,
  document.querySelector('#root'),
);



import React from 'react';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

import { carPropType } from './propTypes';

// hooks are not allowed in class-based component

export class CarEditRow extends React.Component {

  static propTypes = {
    car: carPropType.isRequired,
    onSaveCar: PropTypes.func.isRequired,
    onCancelCar: PropTypes.func.isRequired,
  };

  static defaultProps = {

  };

  state = {
    ...omit(this.props.car, ['id']),
  };

  change = (e) => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value,
    });
  };

  constructor(props) {
    super(props);

    this.makeInputRef = React.createRef();
  }

  componentDidMount() {
    if (this.makeInputRef.current) {
      this.makeInputRef.current.focus();
    }
  }

  componentWillUnmount() {
    console.log('edit car row removed');
  }

  // the above code is transpiled to this:
  // constructor(props) {
  //   super(props);

  //   this.state = {
  //     ...omit(props.car, ['id']),
  //   };

  //   this.change = this.change.bind(this);
  // }

  // change(e) {
  //   this.setState({
  //     [ e.target.name ]: e.target.type === 'number'
  //       ? Number(e.target.value) : e.target.value,
  //   });
  // };

  render() {

    const { onSaveCar: saveCar, onCancelCar: cancelCar } = this.props;

    return <tr>
      <td>{this.props.car.id}</td>
      <td><input type="text" id="make-input" name="make"
          value={this.state.make} onChange={this.change} ref={this.makeInputRef} /></td>
      <td><input type="text" id="model-input" name="model"
          value={this.state.model} onChange={this.change} /></td>
      <td><input type="number" id="year-input" name="year"
          value={this.state.year} onChange={this.change} /></td>
      <td><input type="text" id="color-input" name="color"
          value={this.state.color} onChange={this.change} /></td>
      <td><input type="number" id="price-input" name="price"
          value={this.state.price} onChange={this.change} /></td>
      <td>
        <button type="button" onClick={() => saveCar({
          id: this.props.car.id,
          ...this.state,
        })}>Save</button>
        <button type="button" onClick={cancelCar}>Cancel</button>
      </td>
    </tr>;

  }


}

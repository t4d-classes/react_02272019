import React, { memo } from 'react';
import PropTypes from 'prop-types';

export const ToolHeader = memo(({ headerText }) => {

  console.log('tool header re-rendered');

  return <header>
    <h1>{headerText}</h1>
  </header>;
});

ToolHeader.defaultProps = {
  headerText: 'Tool Header',
};

ToolHeader.propTypes = {
  headerText: PropTypes.string,
};
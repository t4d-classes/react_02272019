import React from 'react';
import PropTypes from 'prop-types';

import { carsPropType } from './propTypes';

import { CarViewRow } from './CarViewRow';
import { CarEditRow } from './CarEditRowStateHooks';

export const CarTable = ({
  cars, editCarId,
  onDeleteCar: deleteCar, onEditCar: editCar,
  onSaveCar: saveCar, onCancelCar: cancelCar,
}) => {

  return <table>
    <thead>
      <tr>
        <th>ID</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>Color</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {cars.map(
        car => car.id === editCarId
          ? <CarEditRow key={car.id} car={car} onSaveCar={saveCar} onCancelCar={cancelCar} />
          : <CarViewRow key={car.id} car={car} onDeleteCar={deleteCar} onEditCar={editCar}  />
      )}
    </tbody>
  </table>;

};

CarTable.propTypes = {
  cars: carsPropType,
  editCarId: PropTypes.number.isRequired,
  onEditCar: PropTypes.func.isRequired,
  onDeleteCar: PropTypes.func.isRequired,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
};

CarTable.defaultProps = {
  cars: [],
};
import React, { useState } from 'react';

import { ToolHeader } from './ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

import { carsPropType } from './propTypes';


export const CarTool = ({ cars: initialCars }) => {

  console.log('car tool re-rendered');

  const [ cars, setCars ] = useState(initialCars.concat());
  const [ editCarId, setEditCarId ] = useState(-1);

  const addCar = (car) => {
    setEditCarId(-1);
    setCars(cars.concat({
      id: Math.max(...cars.map(c => c.id)) + 1,
      ...car,
    }));
  };

  const editCar = (editCarId) => {
    setEditCarId(editCarId);
  };

  const deleteCar = (carId) => {
    setEditCarId(-1);
    setCars(cars.filter(car => car.id !== carId));
  };

  const cancelCar = () => {
    setEditCarId(-1);
  };

  const replaceCar = (car) => {
    const carIndex = cars.findIndex(c => c.id === car.id);
    const newCars = cars.concat();
    newCars[carIndex] = car;
    setCars(newCars);
    setEditCarId(-1);
  };

  return <>
    <ToolHeader headerText="Car Tool" />
    <CarTable cars={cars} editCarId={editCarId}
      onDeleteCar={deleteCar} onEditCar={editCar} 
      onSaveCar={replaceCar} onCancelCar={cancelCar} />
    <CarForm onSubmitCar={addCar} buttonText="Add Car" />
    <br />
    <input type="text" onChange={() => setCars(cars.concat())} />
  </>;
};

CarTool.propTypes = {
  cars: carsPropType,
};

CarTool.defaultProps = {
  cars: [],
};
import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { omit } from 'lodash';

import { carPropType } from './propTypes';

export const CarEditRow = ({ car, onSaveCar: saveCar, onCancelCar: cancelCar }) => {

  const [ carForm, setCarForm ] = useState({ ...omit(car, ['id']) });

  const makeInputRef = useRef(null);

  useEffect(() => {
    if (makeInputRef.current) {
      makeInputRef.current.focus();
    }

    return () => {
      console.log('edit car row removed');
    };
  }, []);
  
  const change = (e) => {
    setCarForm({
      ...carForm,
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value,
    });
  };

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" id="make-input" name="make" ref={makeInputRef}
        value={carForm.make} onChange={change} /></td>
    <td><input type="text" id="model-input" name="model"
        value={carForm.model} onChange={change} /></td>
    <td><input type="number" id="year-input" name="year"
        value={carForm.year} onChange={change} /></td>
    <td><input type="text" id="color-input" name="color"
        value={carForm.color} onChange={change} /></td>
    <td><input type="number" id="price-input" name="price"
        value={carForm.price} onChange={change} /></td>
    <td>
      <button type="button" onClick={() => saveCar({
        id: car.id,
        ...carForm,
      })}>Save</button>
      <button type="button" onClick={cancelCar}>Cancel</button>
    </td>
  </tr>;

};

CarEditRow.propTypes = {
  car: carPropType.isRequired,
  onSaveCar: PropTypes.func.isRequired,
  onCancelCar: PropTypes.func.isRequired,
};

CarEditRow.defaultProps = {
  // car: {},
};
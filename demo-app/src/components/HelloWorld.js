import React from 'react';

export const HelloWorld = () => {

  // return React.createElement('header', null,
  //   React.createElement('h1', null, 'Hello World!'),
  //   React.createElement('h2', null, 'SubTitle'));

  // return <header>
  //   <h1>Hello World!</h1>
  //   <h2>SubTitle</h2>
  //   <span></span>
  // </header>;

  // return React.createElement('h1', null, 'Hello World!');

  return <h1>Hello World!</h1>;

};
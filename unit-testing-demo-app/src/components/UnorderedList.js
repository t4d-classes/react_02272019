import React from 'react';

export const UnorderedList = ({ items, onDeleteItem }) => {
  return <ul>
    {items.map(item =>
      <li key={item}>
        {item}
        <button type="button" onClick={() => onDeleteItem(item)}>Delete</button>
      </li>)}
  </ul>;
};
import React from 'react';

export class ColorForm extends React.Component {

  // is not valid JavaScript
  // Babel transpiler would convert this code into the code below
  state = {
    newColor: '',
  };

  // constructor(props) {
  //   super(props);

    // this.state = {
    //   newColor: '',
    // };

    // this.submitColor = this.submitColor.bind(this);
  //}

  // submitColor() {
  // class arrow functions avoided the use of bind in the constructor
  // class arrow functions are not valid JavaScript
  submitColor = () => {
    this.props.onSubmitColor(this.state.newColor);
    this.setState({
      newColor: '',
    });
  };

  change = e => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value)
        : e.target.value
    });
  }

  render() {
    return <form>
      <div>
        <label htmlFor="new-color-input">New Color:</label>
        <input type="text" id="new-color-input" name="newColor"
          value={this.state.newColor} onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitColor}>{this.props.buttonText}</button>
    </form>;
  }

};
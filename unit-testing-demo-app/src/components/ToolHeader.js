import React from 'react';
import PropTypes from 'prop-types';

export const ToolHeader = (props) => {
  console.log('tool header render');
  return <header>
    <h1>{props.headerText}</h1>
  </header>;
};

ToolHeader.propTypes = {
  headerText: PropTypes.string.isRequired,
};

ToolHeader.defaultProps = {
  headerText: 'Default Tool Header',
};